$(function() {
	var getUsername = window.location.search.substring(1);
	$("#user").html(getUsername);
	var score = "";
	var sortedLocalStorage = SortLocalStorage();
	leaderBoard();

	// for(var i = 0; i < localStorage.length; i++) {
	// 	localStorage.removeItem(localStorage.key(i));
	// }

	function leaderBoard() {
		if(localStorage.length > 0) {
			for(var i = 0; i < 5; i++) {
				var time = sortedLocalStorage[i];

				for(var j = 0; j < localStorage.length; j++) {
					//reference: https://www.kirupa.com/html5/storing_and_retrieving_an_array_from_local_storage.htm
					var retrievedData = localStorage.getItem(localStorage.key(j));
					if (retrievedData) {
						var times = retrievedData;

						for(var k = 0; k < times.length; k++) {
							if(times[k] === time) {
								score += (i + 1) + ". " + localStorage.key(j) + " - " + time + "<br>";
							}
						}
					}
				}
			}
		}
		$("#score").html(score);
	}

	function SortLocalStorage() {
		if(localStorage.length > 0) {
			var localStorageArray = [];
			var count = 0;

			for(var i = 0; i < localStorage.length; i++) {
				//reference: https://www.kirupa.com/html5/storing_and_retrieving_an_array_from_local_storage.htm
				var retrievedData = localStorage.getItem(localStorage.key(i));
				if (retrievedData) {
					var times = retrievedData;

					for(var j = 0; j < times.length; j++) {
						localStorageArray[count] = times[j];
						count++;
					}
				}
			}
			return localStorageArray.sort();
		}
	}	

	var imgrandom = ["src/images/1.png", "src/images/11.png", "src/images/2.png", "src/images/22.png", "src/images/3.png", "src/images/33.png",
						"src/images/4.png", "src/images/44.png", "src/images/5.png", "src/images/55.png", "src/images/6.png", "src/images/66.png",
						"src/images/7.png", "src/images/77.png", "src/images/8.png", "src/images/88.png"];

	/** the code in shuffleImg function was taken from http://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array **/
	function shuffleImg(arr) {
		for(var i = arr.length - 1; i > 0; i--) {
			var j = Math.floor(Math.random() * (i + 1));
			var temp = arr[i];
			arr[i] = arr[j];
			arr[j] = temp;
		}
		return arr;
	}

	function addImage() {
		for(var i = 0; i < imgrandom.length; i++) {
			var angka = i + 1;

			$("#img"+ angka +"").attr("src", imgrandom[i]);
		}
	}

	var flipAgain;

	function backflip(el1, el2) {
		flipAgain = setTimeout(function() {
			flipCard(el1);
			flipCard(el2); }
			, 500);
	}

	function backflipall() {
		flipAgain = setTimeout(flipCardAll(), 1000);
		counter = 16;
	}

	function flipCardAll() {
		$(".card").removeClass('flipped');
	}

	function flipCard(el) {
		$("#"+ el +"").removeClass('flipped');
	}

	function addFlip(el) {
		$("#"+ el +"").addClass('flipped');
	}

	function flip(el) {
		flipAgain = setInterval(addFlip(el), 1000);
	}

	/** the following code was taken from https://codepen.io/davedehaan/pen/owiLn **/
	var timer = 0;
	var timerInterval;
	var ms = document.getElementById('ms');
	var second = document.getElementById('second');
	var minute = document.getElementById('minute');
	var msVal;
	var secondVal;
	var minuteVal;

	function start() {
	  stop();
	  timerInterval = setInterval(function() {
	    timer += 1/60;
	    msVal = Math.floor((timer - Math.floor(timer))*100);
	    secondVal = Math.floor(timer) - Math.floor(timer/60) * 60;
	    minuteVal = Math.floor(timer/60);
	    ms.innerHTML = msVal < 10 ? "0" + msVal.toString() : msVal;
	    second.innerHTML = secondVal < 10 ? "0" + secondVal.toString() : secondVal;
	    minute.innerHTML = minuteVal < 10 ? "0" + minuteVal.toString() : minuteVal;
	  }, 1000/60);
	}

	function stop() {
	  clearInterval(timerInterval);
	}

	/** end of reference **/

	var mulai = false;

	$("#startbtn").click(function() {
		shuffleImg(imgrandom);
		addImage();
		timer = 0;
		ms = document.getElementById('ms');
		second = document.getElementById('second');
		minute = document.getElementById('minute');
		mulai = true;

		sortedLocalStorage = SortLocalStorage();
		score = "";
		leaderBoard();

		$(".back img").css("margin-top", "0px");

		var flipArr = [];
		var flippedId = [];
		var flipped = [];
		var counter = 0;

		$('.card').click(function(){
			if(!mulai) {
				return;
			}

        	flip($(this).attr("id"));

			var count = $(".card.flipped").length;
			console.log("jumlah flipped: " + count);

			var srcimg = $(this).children(".back").children("img").attr("src");
			console.log("srcimg: " + srcimg);

			var id = $(this).attr("id");
			console.log("id: " + id);

			flipArr.push(srcimg);
			console.log("flipArr length: " + flipArr.length);

			flippedId.push(id);
			
			if(flipArr.length == 2) {
				var pic1 = flipArr[0].substring(0, 12);
				var pic2 = flipArr[1].substring(0, 12);
				console.log(pic1);
				console.log(pic2);

				for(var i = 0; i < flippedId.length; i++) {
					console.log(flippedId[i]);
				}
				if(pic1 !== pic2) {
					console.log(flippedId[0]);
					console.log(flippedId[1]);

					backflip(flippedId[0], flippedId[1]);
				}
				else {
					if (flipArr[0] !== flipArr[1]) {
						var ada = false;
						for(var i = 0; i < flipped.length; i++) {
							if(flipArr[0].substring(11, 12) === flipped[i]) {
								ada = true;
								break;
							}
						}
						if(!ada) {
							flipped.push(flipArr[0].substring(11, 12));
							$("#"+ flippedId[0] +"").off("click");
							$("#"+ flippedId[1] +"").off("click");
							counter++;
						}
						console.log(counter);
					}
					if(counter == 8) {
						var jam = $("#hour").text();
						var menit = $("#minute").text();
						var detik = $("#second").text();
						var milidetik = $("#ms").text();
						var time = jam + ":" + menit + ":" + detik + ":" + milidetik;

						var ada = false;

						for(var i = 0; i < localStorage.length; i++) {
							if(localStorage.key(i) == getUsername) {
								//reference: https://www.kirupa.com/html5/storing_and_retrieving_an_array_from_local_storage.htm
								var retrievedData = localStorage.getItem(localStorage.key(i));
								var times = JSON.parse(retrievedData);
								times.push(time);

								localStorage.setItem(getUsername, JSON.stringify(times));
								ada = true;
								break;
							}
						}

						if(!ada) {
							var arrTime = [];
							arrTime.push(time);
							localStorage.setItem(getUsername, JSON.stringify(arrTime));
						}
						sortedLocalStorage = SortLocalStorage();
						score = "";
						leaderBoard();
						counter = 0;
						mulai = false;
						stop();

						$("#startbtn").removeClass("disabled");
					}
				}
				flipArr = [];
				flippedId = [];
			}
        });

        $("#startbtn").addClass("disabled");
		backflipall();

		start();

	});

	$("#logoutbtn").click(function() {
		window.location.href = "guesspic.html";
	});
});